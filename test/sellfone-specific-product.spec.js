/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-specific-product.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-specific-product></sellfone-specific-product>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
