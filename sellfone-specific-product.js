import { html, LitElement } from 'lit-element';
import style from './sellfone-specific-product-styles.js';
import '@catsys/sellfone-product-carousel';
import '@advanced-rest-client/star-rating/star-rating.js';

class SellfoneSpecificProduct extends LitElement {
  static get properties() {
    return {
      productObject:{
        type:Object,
        attribute:"product-object"
      },
      phoneSpecs:{
        type:Array,
        arttribute:'phone-specs'
      },
      extraInfoIcons:{
        type:Array,
        attribute:'extra-info-icons'
      },
      phoneDetails:{
        type:Array,
        attribute:'phone-details'
      }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.phoneSpecs=[
      {iconUrl:"https://img.icons8.com/office/40/000000/multiple-cameras.png",title:"Camara",secondaryTitle:"Frontal",secondaryDescription:"0.3MP",thirdTitle:"Trasera",thirdDescription:"10MP"},
      {iconUrl:"https://img.icons8.com/office/40/000000/camcorder.png",title:"Video",secondaryTitle:"Extras",secondaryDescription:"Full hd",thirdTitle:"",thirdDescription:""},
      {iconUrl:"https://img.icons8.com/office/40/000000/hdd--v1.png",title:"Memoria",secondaryTitle:"Ram",secondaryDescription:"1 GB",thirdTitle:"Interna",thirdDescription:"16 GB"},
      {iconUrl:"https://img.icons8.com/color/48/000000/processor.png",title:"Procesador",secondaryTitle:"Velocidad",secondaryDescription:"1 GHz",thirdTitle:"Chip",thirdDescription:"descripcion chip"},
      {iconUrl:"https://img.icons8.com/ultraviolet/40/000000/wifi.png",title:"Conectividad",secondaryTitle:"Red",secondaryDescription:"3G",thirdTitle:"USB",thirdDescription:"Micro USB 2.0"}
    ]
    this.extraInfoIcons=[
      {src:'https://img.icons8.com/color/96/000000/unlock-2.png',text:'Desbloqueado'},
      {src:'https://img.icons8.com/dusk/50/000000/guarantee.png',text:'Garantía'}, 
      {src:'https://img.icons8.com/cotton/50/000000/bill.png',text:'Factura'},
      {src:'https://img.icons8.com/dusk/50/000000/cell-phone.png',text:'Reajuste de fabrica'}
      
    ],
    this.phoneDetails=[
      {
        src:'https://img.icons8.com/dusk/50/000000/cell-phone.png',
        text:'Pantalla', 
        subText:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.'
    },{
      src:'https://img.icons8.com/dusk/50/000000/cell-phone.png',
      text:'Lado', 
      subText:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.'
  },{
    src:'https://img.icons8.com/dusk/50/000000/cell-phone.png',
    text:'Trasero', 
    subText:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.'
}
    ]
  }

  render() {
    return html`
        <div class="mainContainer">
           <div class="container">
            <sellfone-product-carousel></sellfone-product-carousel>
           </div>
           <div class="container2">
            <h2 class="title">${this.productObject.title}</h2>
            <star-rating class="rating" value="${this.productObject.rating}" readonly></star-rating>
              <p class="descriptor">Precio:</p>
              <div class="priceContainer">
                <p class="price"><span class="currency">${this.productObject.currency}</span>${this.productObject.price}</p>
                <button class="buyButton" @click="${this.addProduct}">Comprar</button>
              </div>
              <div class="priceContainer">
                <p class="descriptor">12 meses sin intereses <span><a href="#">ver opciones</a></span></p>
                <p class="over">Producto en existencias!</p>
              </div>
              <p class="description">Precio nuevo: <span class="currency2">${this.productObject.currency} ${this.productObject.priceAsNew}</span></p>
              <p class="descriptor">Descripción:</p>
              <p class="description">${this.productObject.description}</p>
              <p class="descriptor">Método de envío:</p>
              <div class="shipmentInputContainer">
                 <input class="shipmentInput" placeholder="Ingrese su CP">
                 <button class="calculateButton">Calcular</button>
              </div>
           </div>
        </div>
        <div class="bottomDescriptionContainer">
              <p class="sellfoneTitle">¿Por que comprar con Sellfone?</p>
              <div class="textContainer">
                  <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
          <div class="iconContainer">
                ${this._repeatIcons()}
          </div>
          <p class="sellfoneTitle">¿Tienes dudas sobre los télefonos Open Box?</p>
              <div class="textContainer">
                  <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
              <div class="physicDetails">
                ${this._repeatPhysicDetails()}
              </div>
              <div class="boxContentContainer">
                  <img class="boxIcon" src="https://cdn4.iconfinder.com/data/icons/web-design-and-development-4-8/48/186-512.png" alt="">
                  <div class="boxContentText">
                     <p class="sellfoneTitle">¿Qué viene en la caja?</p>
                     <p class="boxText">1 Iphone Xs 512 gb negro</p>
                  </div>
              </div>
              ${this._renderPhoneSpecs()}
        </div>
      `;
    }

    updated(updatedProperties){
      if(updatedProperties.has("productObject")){
        this.shadowRoot.querySelector("sellfone-product-carousel").imagesArray=this.productObject.images;
      }
    }

    _renderPhoneSpecs(){
      return html`
        <div class="specsContainer">
            ${this.phoneSpecs.map(specs=>{
                return html`<div class="specBox">
                    <img class="specIcon" src="${specs.iconUrl}">
                    <p class="boxTitle">${specs.title}</p>
                    <p class="boxSubTitle">${specs.secondaryTitle}:<span class="textDescription">${specs.secondaryDescription}</span></p>
                    <p class="boxSubTitle">${specs.thirdTitle}:<span class="textDescription">${specs.thirdDescription}</span></p>
                </div>`
            })}
        </div>
      `
    }

    _repeatIcons(){
     return html`
      ${this.extraInfoIcons.map((icons)=>{
        return html`
          <div class="iconsInfo">
             <p class="iconsText">
               ${icons.text}
             </p>
             <img class="icon" src="${icons.src}" alt="${icons.text}">
          </div>
        `;
      })}
     `;
    }

    _repeatPhysicDetails(){
      return html`
      ${this.phoneDetails.map((detail)=>{
          return html`
          <div class="physicContainer">
            <div class="image">
            
            </div>
            <p class="physicTitle">
              ${detail.text}
            </p>
            <p class="physicText">
            ${detail.subText}
            </p>
          </div>
          `;
      })}
      `;
    }



    addProduct(event){
      this.dispatchEvent(new CustomEvent("product-added",{
        bubbles:false,
        composed:false,
        detail:this.productObject
      }));
    }
}

window.customElements.define("sellfone-specific-product", SellfoneSpecificProduct);
